# [Longest Substring Without Repeating Characters][title]

## Description

Given a string, find the length of the **longest substring** without repeating characters.

**Examples:**

Given `"abcabcbb"`, the answer is `"abc"`, which the length is 3.

Given `"bbbbb"`, the answer is `"b"`, with the length of 1.

Given `"pwwkew"`, the answer is `"wke"`, with the length of 3. Note that the answer must be a **substring**, `"pwke"` is a *subsequence*and not a substring.

**Tags:** Hash Table, Two Pointers, String


## 思路

題意是計算不帶重複字符的最長子字符串的長度，開闢一個散列數組來存儲該字符上次出現的位置，比如`hash [a] = 3`就是代表`a`字符前一次出現的索引在3中，遍歷該字符串，獲取到上次出現的最大索引（只能向前，不能退後），與當前的索引做差獲取的就是本次所需長度，從中迭代出最大值就是最終答案。

```java
class Solution {
    public int lengthOfLongestSubstring(String s) {
        int len;
        if (s == null || (len = s.length()) == 0) return 0;
        int preP = 0, max = 0;
        int[] hash = new int[128];
        for (int i = 0; i < len; ++i) {
            char c = s.charAt(i);
            if (hash[c] > preP) {
                preP = hash[c];
            }
            int l = i - preP + 1;
            hash[c] = i + 1;
            if (l > max) max = l;
        }
        return max;
    }
}
```


## 结语

如果你同我一样热爱数据结构、算法、LeetCode，可以关注我GitHub上的LeetCode题解：[awesome-java-leetcode][ajl]



[title]: https://leetcode.com/problems/longest-substring-without-repeating-characters
[ajl]: https://github.com/Blankj/awesome-java-leetcode

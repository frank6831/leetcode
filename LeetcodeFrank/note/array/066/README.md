# [Plus One][title]

## Description

Given a non-negative integer represented as a **non-empty** array of digits, plus one to the integer.

You may assume the integer do not contain any leading zero, except the number 0 itself.

The digits are stored such that the most significant digit is at the head of the list.

**Tags:** Array, Math


## 思路

題意是給你一個數字數組，高位在前，並且首位不為0除非這個數組就是`[0]`，讓你給該數組低位加一求其結果，那麼我們就模擬小學數學那樣進位去算即可，如果一直進位到首位，這種情況也就是都是由9組成的數組，此時我們只要new出一個多一個長度的數組即可，並把第0個元素賦1即可。

```java
class Solution {
    public int[] plusOne(int[] digits) {
        int p = digits.length - 1;
        if (digits[p] < 9) {
            digits[p] = ++digits[p];
        } else {
            do {
                digits[p--] = 0;
            } while (p >= 0 && digits[p] == 9);
            if (digits[0] != 0) {
                ++digits[p];
            } else {
                digits = new int[digits.length + 1];
                digits[0] = 1;
            }
        }
        return digits;
    }
}
```


## 结语

如果你同我一样热爱数据结构、算法、LeetCode，可以关注我GitHub上的LeetCode题解：[awesome-java-leetcode][ajl]



[title]: https://leetcode.com/problems/plus-one
[ajl]: https://github.com/Blankj/awesome-java-leetcode

package com.frank.array._283_E;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/7/18
 *     title : 283. Move Zeroes
 * </pre>
 */
public class Solution {
    
	/**
	Given an array nums, write a function to move all 0's to the end of it 
	while maintaining the relative order of the non-zero elements.

	Example:
	
	Input: [0,1,0,3,12]
	Output: [1,3,12,0,0]
	Note:
	
	You must do this in-place without making a copy of the array.
	Minimize the total number of operations.
	*/
	// 題目意思：輸入為一個陣列，要將該陣列中所有為0的元素移動至最後面進行存放
	/*
	  思路：
	  使用Two pointer進行實現，將兩個雙指標索引放在起始的位置，
	  並利用其中一個指標進行偵測，若遇到0則向右移一格，若不是則將兩個指標索引之值進行互換。
	 1.將兩個指標索引放置於頭的位置(int i = 0, j = 0;)。
	 2.利用其中一個指標索引進行偵測，若遇到0則向右移一格(if (nums[j] == 0))。
	 3.若偵測到不為0，則將兩指標索引之值進行互換，並且將兩個指標索引向右移一格(程式中else的部分)。
	 */
	public void moveZeroes(int[] nums) {
		 	int i = 0, j = 0;
	        int tmp;
	        
	        while (j < nums.length) {
	            if  (nums[j] == 0) {
	                j++;
	            } else {
	                tmp = nums[j];
	                nums[j] = nums[i];
	                nums[i] = tmp;
	                i++;
	                j++;                
	            }                 
	        }
    }
	
    public static void main(String[] args) {
    	Solution solution = new Solution();
        int sample[] = {0,1,0,3,12};
        
        solution.moveZeroes(sample);
        System.out.println(Arrays.toString(sample));
    }
}

package com.frank.array._035;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/02/08
 *     title  : 35. Search Insert Position
 * </pre>
 */
public class Solution {
    
	/**
	Given a sorted array and a target value, return the index if the target is found. 
	If not, return the index where it would be if it were inserted in order.
	You may assume no duplicates in the array.
	
	Example 1:
	Input: [1,3,5,6], 5
	Output: 2
	
	Example 2:
	Input: [1,3,5,6], 2
	Output: 1
	
	Example 3:
	Input: [1,3,5,6], 7
	Output: 4
	
	Example 4:
	Input: [1,3,5,6], 0
	Output: 0
	 */
	
	//题意是让你从一个没有重复元素的已排序数组中找到插入位置的索引。
	//因为数组已排序，所以我们可以想到二分查找法，因为查找到的条件是找到第一个等于或者
	//大于`target`的元素的位置，所以二分法略作变动即可。
	
	
	
	//思路：迴圈比對，當取出的item >= target時，就回傳當下item的index
	public int searchInsert(int[] nums, int target) {
		for (int i=0;i<nums.length;i++) {
			if (nums[i] >= target) {
				return i;
			}
		}
        return nums.length;
    }
	

    public static void main(String[] args) {
        Solution solution = new Solution();
        int sample[] = {1,3,5,6};
        System.out.println(solution.searchInsert(sample, 2));
    }
}

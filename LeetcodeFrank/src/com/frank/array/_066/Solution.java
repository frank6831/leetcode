package com.frank.array._066;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/11/12
 *     desc  : 66. Plus One
 * </pre>
 */

/**
Given a non-empty array of digits representing a non-negative integer, 
plus one to the integer.
The digits are stored such that the most significant digit is 
at the head of the list, and each element in the array contain 
a single digit.

You may assume the integer does not contain any leading zero, 
except the number 0 itself.

Example 1:

Input: [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.

Example 2:
Input: [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.
*/
public class Solution {
    
	/*
	 正確思路分析：
	 如果數組的最後一位不是9的話，那麼直接最後一位自身增加1返回數組即可；
	 如果碰到是9的情況，則需要考慮進位的情況，從後往前遍歷循環，如果為為9則賦值為0，
	 前一位加1，如果不為9則加一返回，或者循環結束，如果循環結束還沒有return，
	 則可以判斷每位都是9，則可以全賦值為0，前面增加一個1。
	 */
	
	public int[] plusOne(int[] digits) {
		int n = digits.length;
        for (int i = n - 1; i >= 0; --i) {
            if (digits[i] < 9) {
                ++digits[i];
                return digits;
            } else {
            		digits[i] = 0;
            }
        }
        int[] res = new int[n + 1];
        res[0] = 1;
        return res;
    }
	

    public static void main(String[] args) {
        Solution solution = new Solution();
        int sample[] = {9,1,3};
        System.out.println(Arrays.toString(solution.plusOne(sample)));
    }
}

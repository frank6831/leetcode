package com.frank.array._088;

import java.util.Arrays;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/11/12
 *     desc  : 88. Merge Sorted Array
 * </pre>
 */

/**
Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 
as one sorted array.

Note:

The number of elements initialized in nums1 and nums2 are m and n respectively.
You may assume that nums1 has enough space 
(size that is greater or equal to m + n) to hold additional elements from nums2.

Example:
Input:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

Output: [1,2,2,3,5,6]
*/
public class Solution {
    
	public void merge(int A[], int m, int B[], int n) {
        while(m > 0 && n > 0){
            if(A[m-1] > B[n-1]){
                A[m+n-1] = A[m-1];
                m--;
            }else{
                A[m+n-1] = B[n-1];
                n--;
            }
        }
 
        while(n > 0){
            A[m+n-1] = B[n-1];
            n--;
        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        int sample[] = {1,2,3,0,0,0};
        int sample2[] = {2,5,6};
        solution.merge(sample, 3, sample2, 3);
        System.out.println(Arrays.toString(sample));
    }
}

package com.frank.array._026;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/02/06
 *     title   : 26. Remove Duplicates from Sorted Array
 * </pre>
 */
public class Solution {
    
	/**
		 # [Remove Duplicates from Sorted Array]
		 ## Description
		
		Given a sorted array, remove the duplicates in-place such that each element appear 
		only once and return the new length.
			
		Do not allocate extra space for another array, you must do this by modifying 
		the input array in-place with O(1) extra memory.
		
		**Examples:**
		Given nums = [1,1,2],
		
		Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
		It doesn't matter what you leave beyond the new length.
		
		**Tags:** Array, Two Pointers 
	 */
	// 題意是讓你從一個有序的數組中移除重複的元素，並返回之後數組的長度
	
	/*
	 * 思路：
	 * 
	 */
	
	public int removeDuplicates(int[] nums) {
		int len = nums.length;
		int tail = 1;
		for (int i = 1; i < len;i++) {
			if (nums[i-1] != nums[i]) {
				nums[tail++] = nums[i];
			}
		}
		return tail;
    }
	
	public int removeDuplicates2(int[] nums) {
		if(nums == null || nums.length == 0) return 0;
	    if(nums.length == 1) return 1;
	    int count = 0;
	    for(int i = 1 ; i < nums.length ; i++){
	        if(nums[count] != nums[i]){
	            count++;
	            nums[count] = nums[i];
	        }
	    }    
	    return ++count;
	}

    public static void main(String[] args) {
        Solution solution = new Solution();
        int sample[] = {1,2,4,4,5,5};
        
        System.out.println(solution.removeDuplicates2(sample));
    }
}

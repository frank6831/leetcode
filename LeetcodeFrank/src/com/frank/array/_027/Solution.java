package com.frank.array._027;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/02/08
 *     title : 27. Remove Element
 * </pre>
 */
public class Solution {
    
	/**
	Given an array nums and a value val, remove all instances of that value in-place and return the new length.
	Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
	The order of elements can be changed. It doesn't matter what you leave beyond the new length.

	Example 1:
	Given nums = [3,2,2,3], val = 3,
	Your function should return length = 2, with the first two elements of nums being 2.
	It doesn't matter what you leave beyond the returned length.

	Example 2:
	Given nums = [0,1,2,2,3,0,4,2], val = 2,
	Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4.
	Note that the order of those five elements can be arbitrary.
	
	It doesn't matter what values are set beyond the returned length.
	Clarification:
	Confused why the returned value is an integer but your answer is an array?
	Note that the input array is passed in by reference, which means modification to the 
	input array will be known to the caller as well.
	
	Internally you can think of this:
	
	// nums is passed in by reference. (i.e., without making a copy)
	int len = removeElement(nums, val);
	
	// any modification to nums in your function would be known by the caller.
	// using the length returned by your function, it prints the first len elements.
	for (int i = 0; i < len; i++) {
	    print(nums[i]);
	}
	 */
	
	/*
	 題意是移除數組中值等於`val`的元素，並返回之後數組的長度，
	 並且題目中指定空間複雜度為O(1)，我的思路是用`tail`標記尾部，
	 遍歷該數組時當索引元素不等於`val`時，`tail`加一，尾部指向當前元素，最後返回`tail`即可。
	 */
	public int removeElement(int[] nums, int val) {
        int pos = 0;
		for (int i =0; i < nums.length;i++) {
			if (nums[i] != val){
                nums[pos] = nums[i];
                pos++;
            }
		}
		return pos;
    }
	
	public int removeElement2(int[] nums, int val) {
        int tail = 0;
		for (int i =0; i < nums.length;i++) {
			if (nums[i] != val) {
				nums[tail++] = nums[i];
			}
		}
		return tail;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        int sample[] = {3,2,2,3,4};
        
        System.out.println(solution.removeElement(sample, 2));
        //System.out.println("sample:" + sample.length);
        for (int i =0; i < sample.length;i++) {
        	System.out.println("sample item:" + sample[i]);
		}
        
    }
}

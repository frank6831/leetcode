package com.frank.array._167;

import java.util.Arrays;
import java.util.HashMap;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/7/8
 *     title : 167. Two Sum II - Input array is sorted
 * </pre>
 */
public class Solution {
    
	/**
	Given an array of integers that is already sorted in ascending order, 
	find two numbers such that they add up to a specific target number.

	The function twoSum should return indices of the two numbers such 
	that they add up to the target, where index1 must be less than index2.

	Note:
	
	Your returned answers (both index1 and index2) are not zero-based.
	You may assume that each input would have exactly one solution and 
	you may not use the same element twice.
	
	Example:
	Input: numbers = [2,7,11,15], target = 9
	Output: [1,2]
	Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
	*/
	/*
	 * 解法一：map解法
	 */
	public static int[] twoSum(int[] numbers, int target) {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        int size = numbers.length;
        for (int i =0; i<size; i++) {
	        	if (map.containsKey(numbers[i])) {
	             return new int[] {map.get(numbers[i]), i};
	        }
        	 	map.put(target - numbers[i], i);
        }
        return null;
    }
	
	/*
	 與1. Tow Sum類似，這題的輸入是有序數組，限定了一定會有解，用雙指針來做，
	 定義左右兩個指針，左指針指向第一個數，右指針指向最後一個數，
	 然後用這兩個數的和與Target比較，如果比Target小，左指針向右移一位，
	 如果比Target大，右指針向左移一位。然後再進行比較，直到找到或者兩個指針相遇為止。

	 注意：左右指針是從0到 len(numbers)-1， 輸出結果是從1開始的index.
	 Time: O(n) Space: O(1)
	 */
	 public static int[] twoSumByTwoPointer(int[] numbers, int target) {
	        int[] res = new int[2];
	        int i = 0, j = numbers.length - 1;
	        while (i < j) {
	            if ((numbers[i] + numbers[j]) > target) {
	                j--;
	            } else if ((numbers[i] + numbers[j]) < target) {
	                i++;
	            } else {
	                res[0] = i + 1;
	                res[1] = j + 1;
	                break;
	            }           
	        }
	        return res;       
	 }
	
	
    public static void main(String[] args) {
        int num1[] = {4,5,8,12};
        int num2[] = {9,4,9,8,4};
        int num3[] = {1,2,2,1};
        int num4[] = {2,2};
        
        int result[] = twoSumByTwoPointer(num1, 9);
        System.out.println(Arrays.toString(result));
    }
}

package com.frank.array._053;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/11/12
 *     desc  : 53.Maximum Subarray
 * </pre>
 */

/**
# [Maximum Subarray]

## Description
Find the contiguous subarray within an array (containing at least one number) which has the largest sum.
For example, given the array [-2,1,-3,4,-1,2,1,-5,4],
the contiguous subarray [4,-1,2,1] has the largest sum = 6.

More practice:

If you have figured out the O(*n*) solution, try coding another solution 
using the divide and conquer approach, which is more subtle.
Tags: Array, Dynamic Programming, Divide and Conquer
*/
public class Solution {
    
	/*
	題意是求數組中子數組的最大和，這種最優問題一般第一時間想到的就是動態規劃，
	我們可以這樣想，當部分序列和大於零的話就一直加下一個元素即可，
	並和當前最大值進行比較，如果出現部分序列小於零的情況，
	那肯定就是從當前元素算起。其轉移方程就是
	dp[i] = nums[i] + (dp[i - 1] > 0 ? dp[i - 1] : 0);，
	由於我們不需要保留dp狀態，故可以優化空間複雜度為1，
	即'dp = nums[i] + (dp > 0 ? dp : 0);'。
	 */
	
	public int maxSubArray(int[] nums) {
        int len = nums.length, dp = nums[0], max = dp;
        for (int i = 1; i < len; ++i) {
            dp = nums[i] + (dp > 0 ? dp : 0);
            if (dp > max) max = dp;
        }
        return max;
    }
	
	/*
	 本題為最優解問題，可利用動態規劃思路求解。從頭到尾遍歷每一個數組元素，
	 如何前面元素的和為正，則加上本元素的值繼續搜索；如何前面元素的和為負，
	 則此元素開始新的和計數。整個過程中要注意更新和的最大值。
	 */
	public int maxSubArray2(int[] nums) {
        if(nums == null) return 0;
        int sum = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0 ; i < nums.length; i++){
            if (sum + nums[i] > nums[i]) {
                sum += nums[i];
            } else {
                sum = nums[i];
            }
            if (sum > max) {
                max = sum;
            }
        }
        return max;       
    }
	

    public static void main(String[] args) {
        Solution solution = new Solution();
        int sample[] = {-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(solution.maxSubArray(sample));
    }
}

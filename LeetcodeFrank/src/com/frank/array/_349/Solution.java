package com.frank.array._349;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
/**
 * <pre>
 *     author: frank
 *     time  : 2019/5/10
 *     title : 349. Intersection of Two Arrays
 * </pre>
 */
public class Solution {
    
	/**
	Given two arrays, write a function to compute their intersection.

	Example 1:
	Input: nums1 = [1,2,2,1], nums2 = [2,2]
	Output: [2]
	
	Example 2:
	Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
	Output: [9,4]
	Note:
	
	Each element in the result must be unique.
	The result can be in any order.
	*/
	/*
	 * 解法一：暴力解，兩個迴圈一個一個比對，利用HashSet去重。
	 */
	public static int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> resultSet = new HashSet<>();
		for (int i = 0; i< nums1.length;i++) {
			for (int j =0; j < nums2.length; j++) {
				if (nums1[i] == nums2[j]) {
					resultSet.add(nums1[i]);
				}
			}
		}
		 int[] arr = new int[resultSet.size()];
		 int i = 0;
		 for (Integer val : resultSet) {
			 arr[i++] = val;
		 }
		return arr;
    }
	
	/*
	 "交集"即同時出現在兩個數組中的元素的集合。那麼如何來判斷"元素同時出現在兩個數組中"呢？
	 實際只需要遍歷其中一個數組，對其每個元素檢查其是否在另一個數組中，
	 為了加快這個“檢查”的速度，不妨把另一個數組首先轉換為HashMap，
	 這樣每次“檢查”的時間複雜度就只有O(1)了。

	 接下來還有一個問題，如果兩個數組中含有重複元素的話該如何處理呢？
	 根據此題題意，輸出的交集中應該不含有重複元素，那麼遍歷的過程中，
	 中間結果也用HashMap保存就好了，這樣可以剔除重複元素。
	 */
	public int[] intersection2(int[] nums1, int[] nums2) {
        Set<Integer> nums1Set = new HashSet<>();
        for (int num : nums1) {
            nums1Set.add(num);
        }
        Set<Integer> intersect = new HashSet<>();
        for (int num : nums2) {
            if (nums1Set.contains(num)) {
                intersect.add(num);
            }
        }
        int[] intersectArray = new int[intersect.size()];
        int i = 0;
        for (int num : intersect) {
            intersectArray[i++] = num;
        }
        return intersectArray;
    }
	
    public static void main(String[] args) {
        int num1[] = {4,9,5};
        int num2[] = {9,4,9,8,4};
        int num3[] = {1,2,2,1};
        int num4[] = {2,2};
        
        int result[] = intersection(num1, num2);
        System.out.println(Arrays.toString(result));
    }
}

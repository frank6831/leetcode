package com.frank.string._014;

/**
 * <pre>
 *     author: Frank
 *     blog  : http://blankj.com
 *     time  : 2018/02/05
 *     desc  :
 * </pre>
 */
public class Solution {
	public String longestCommonPrefix(String[] strs) {
		if (strs == null || strs.length == 0) {
			return "";
		}
		int len = strs.length;
		int minLen = 0x7fffffff;
		for (String str : strs) {
			minLen = Math.min(minLen, str.length());
		}

		for (int j = 0; j < minLen; j++) {// 最多比較到幾個字元
			for (int i = 0; i < len; i++) {// 字串array index
				if (strs[0].charAt(j) != strs[i].charAt(j)) {
					return strs[0].substring(0, j);
				}
			}
		}
		return strs[0].substring(0, minLen);
	}

	/**
	 * Horizontal scanning
	 * 
	 * @param strs
	 * @return
	 */
	public String longestCommonPrefixHorizontal(String[] strs) {
		if (strs.length == 0)
			return "";
		String prefix = strs[0];
		for (int i = 1; i < strs.length; i++)
			while (strs[i].indexOf(prefix) != 0) {
				prefix = prefix.substring(0, prefix.length() - 1);
				if (prefix.isEmpty())
					return "";
			}
		return prefix;
	}

	public static void main(String[] args) {
		Solution solution = new Solution();
		String sample[] = { "ab", "abc", "abcde" };

		System.out.println("result:" + solution.longestCommonPrefix(sample));
	}
}

package com.frank.string._038;

/**
 * <pre>
 *     author: Frank
 *     blog  : http://blankj.com
 *     time  : 2018/02/05
 *     desc  :
 * </pre>
 */
public class Solution {
	
	/*
	1. 1
	2. 11
	3. 21
	4. 1211
	5. 111221 
	6. 312211
	7. 13112221
	8. 1113213211
	9. 31131211131221
	10. 13211311123113112211
	 */
	
	public String countAndSay(int n) {
		if (n <= 0) {  
		    return null;  
		}  
		String s = "1";  
		int num = 1;  
		for (int j = 0; j < n - 1; j++) {  
		    StringBuilder sb = new StringBuilder();  
		    for (int i = 0; i < s.length(); i++) {  
		        if (i < s.length() - 1 && s.charAt(i) == s.charAt(i + 1)) {  
		            num++;  
		        } else {  
		            sb.append(num + "" + s.charAt(i));  
		            num = 1;  
		        }  
		    }  
		    s = sb.toString();  
		}  
		return s;  
	}
	
	public static void main(String[] args) {
		Solution solution = new Solution();
		System.out.println("result:" + solution.countAndSay(4));
	}
}

package com.frank.string._003;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <pre>
 *     author: frank
 *     blog  : http://blankj.com
 *     time  : 2017/10/11
 *     desc  : 3. Longest Substring Without Repeating Characters
 * </pre>
 */
public class Solution {
	/*
	 Given a string, find the length of the longest substring without repeating characters.

	Example 1:
	Input: "abcabcbb"
	Output: 3 
	Explanation: The answer is "abc", with the length of 3.
	 
	Example 2:
	Input: "bbbbb"
	Output: 1
	Explanation: The answer is "b", with the length of 1.
	
	Example 3:
	Input: "pwwkew"
	Output: 3
	Explanation: The answer is "wke", with the length of 3. 
	             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
	 */
	
    public int lengthOfLongestSubstring(String s) {
        int len;
        if (s == null || (len = s.length()) == 0) return 0;
        int preP = 0, max = 0;
        int[] hash = new int[128];
        for (int i = 0; i < len; ++i) {
            char c = s.charAt(i);
            System.out.println("c:"+ c + ", preP:" + preP);
            if (hash[c] > preP) {
                preP = hash[c];
            }
            int l = i - preP + 1;
            hash[c] = i + 1;
            System.out.println("l:"+ l + ", hash[c]:" + i+1);
            if (l > max) max = l;
        }
        return max;
    }
    
    public int lengthOfLongestSubstringSet(String s) {
        int n = s.length();
        Set<Character> set = new HashSet<>();
        int ans = 0, i = 0, j = 0;
        System.out.println("length:"+ n);
        while (j < n) {
            // try to extend the range [i, j]
            if (!set.contains(s.charAt(j))){
            		System.out.println("charAt:"+ s.charAt(j));
                set.add(s.charAt(j++));
                ans = Math.max(ans, set.size());
            }
            else {
                set.remove(s.charAt(i++));
            }
        }
        return ans;
    }
    
    public int lengthOfLongestSubstringMap(String s) {
        int n = s.length(), ans = 0;
        Map<Character, Integer> map = new HashMap<>(); // current index of character
        // try to extend the range [i, j]
        for (int j = 0, i = 0; j < n; j++) {
            if (map.containsKey(s.charAt(j))) {
                i = Math.max(map.get(s.charAt(j)), i);
            }
            ans = Math.max(ans, j - i + 1);
            map.put(s.charAt(j), j + 1);
        }
        return ans;
    }
    
    
    public int lengthOfLongestSubstringTable(String s) {
        int n = s.length(), ans = 0;
        int[] index = new int[128]; // current index of character
        // try to extend the range [i, j]
        for (int j = 0, i = 0; j < n; j++) {
            i = Math.max(index[s.charAt(j)], i);
            ans = Math.max(ans, j - i + 1);
            index[s.charAt(j)] = j + 1;
        }
        return ans;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.lengthOfLongestSubstringMap("abcabcbb"));
        //System.out.println(solution.lengthOfLongestSubstringSet("bbbbb"));
        //System.out.println(solution.lengthOfLongestSubstring("pwwkew"));
        //System.out.println(solution.lengthOfLongestSubstring("Abcabcbb"));
    }
}

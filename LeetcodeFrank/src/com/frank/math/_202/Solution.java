package com.frank.math._202;

import java.util.Arrays;
import java.util.HashSet;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/03/10
 *     title : 202. Happy Number
 * </pre>
 */
public class Solution {

	/**
	 Write an algorithm to determine if a number is "happy".

	A happy number is a number defined by the following process: 
	Starting with any positive integer, replace the number by the sum of the squares of its digits, 
	and repeat the process until the number equals 1 (where it will stay), or it loops endlessly 
	in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.
	
	Example: 
		Input: 19
		Output: true
		Explanation: 
		1^2 + 9^2 = 82
		8^2 + 2^2 = 68
		6^2 + 8^2 = 100
		1^2 + 0^2 + 0^2 = 1
	 */

	/*
	 解題：
	 把讀入的數字弄成字串，之後就可以針對各個位元作平方。平方和拿到後判斷是不是1，是1的話就跳出去且印出他是happy number的訊息。
	 不是的話就先看一下是不是一開始的數字(S0)，如果是的話就是unhappy number並印出訊息，不是的話就繼續找下一個。
	 */
	public boolean isHappy(int n) {
        HashSet<Integer> set =new HashSet<>();
        while (n!=1) {
            n= squareSum(n);
            if (set.contains(n)) return false;
            set.add(n);
        }
        return true;
    }
	
    public int squareSum(int n){
        int sum=0;
        while (n>0) {
            sum+=(n%10)*(n%10);
            n/=10;
        }
        return sum;
    }
	
	public static void main(String[] args) {
		Solution solution = new Solution();
		System.out.println("is Happy number :" +solution.isHappy(36));
	}
}

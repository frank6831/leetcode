package com.frank.math._169;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * <pre>
 *     author: frank
 *     time  : 2018/02/26
 *     title  : 169. Majority Element
 * </pre>
 */
public class Solution {

	/**
	 * Given an array of size n, find the majority element. The majority element is
	 * the element that appears more than ⌊ n/2 ⌋ times. You may assume that the
	 * array is non-empty and the majority element always exist in the array.
	 * 
	 * Example 1:
	 * Input: [3,2,3] Output: 3 
	 * 
	 * Example 2:
	 * Input: [2,2,1,1,1,2,2] Output: 2
	 */

	/*
	 * 思路1：将数组排序，则中间的那个元素一定是多数元素
	 */
	public int majorityElementSolution1(int[] nums) {
		Arrays.sort(nums);
		return nums[nums.length / 2];
	}

	/*
	 * 思路2：利用HashMap来记录每个元素的出现次数
	 */
	public int majorityElementSolution2(int[] nums) {
		HashMap<Integer, Integer> map = new HashMap<>();

		for (int i = 0; i < nums.length; i++) {
			if (!map.containsKey(nums[i])) {
				map.put(nums[i], 1);
			} else {
				int values = map.get(nums[i]);
				map.put(nums[i], ++values);
			}
		}
		int n = nums.length / 2;
		Set<Integer> keySet = map.keySet();
		Iterator<Integer> iterator = keySet.iterator();
		while (iterator.hasNext()) {
			int key = iterator.next();
			int value = map.get(key);
			if (value > n) {
				return key;
			}
		}
		return 0;
	}

	/*
	 * 思路3：Moore voting algorithm--每找出两个不同的element，
	 * 就成对删除即count--，最终剩下的一定就是所求的。时间复杂度：O(n)
	 */
	public int majorityElementSolution3(int[] nums) {
		int elem = 0;
		int count = 0;

		for (int i = 0; i < nums.length; i++) {
			if (count == 0) {
				elem = nums[i];
				count = 1;
			} else {
				if (elem == nums[i])
					count++;
				else
					count--;
			}
		}
		return elem;
	}

	public static void main(String[] args) {
		Solution solution = new Solution();
		int sample[] = { 2, 2, 1, 1, 1, 2, 2 };
		System.out.println(solution.majorityElementSolution3(sample));
	}
}
